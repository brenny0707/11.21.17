import React from 'react';
import TableRow from './table_row';
import TableCol from './table_col';
import ColHeader from './col_header';
import RowHeader from './row_header';
import Reservation from './reservation';
import {timeRange} from './util_functions';

class TableGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tables: props.tables,
      timeData: props.timeData,
      testReservations: props.testReservations,
    };
  }


  render() {
    const tables = this.state["tables"];
    const tableArray = Object.keys(tables);
    const { testReservations } = this.state;
    const { compiledTimes, timeDuration, startTime, timeRanges } = this.state["timeData"];
    let cols = timeRanges.length + 1;
    let rows = tableArray.length + 1;
    const colSize = `90px`;
    const rowSize = `40px`;
    const headerZIndex = 10;

    const gridStyle = {
      display: "grid",
      gridTemplateRows: `repeat(${rows}, ${rowSize})`,
      gridTemplateColumns: `repeat(${cols}, ${colSize})`,
      minWidth: `${colSize.split("px")[0] * cols}px`,
    };

    const rowsStyle = {
      gridColumn: `1 / ${cols}`,
      gridRow: `2 / ${rows}`,
      display: "grid",
      gridTemplateRows: `repeat(${rows - 1}, ${rowSize})`,
      gridTemplateColumns: `repeat(${cols}, ${colSize})`,
    };

    const colsStyle = {
      gridColumn: `2 / ${cols}`,
      gridRow: `1 / ${rows}`,
      display: "grid",
      gridTemplateRows: `repeat(${rows}, ${rowSize})`,
      gridTemplateColumns: `repeat(${cols - 1}, ${colSize})`,
    };

    const zeroStyle = {
      gridRow: `1 / span 1`,
      gridColumn: `1 / span 1`,
      position: "fixed",
      left: "0px",
      top: "0px",
      backgroundColor: "white",
      zIndex: headerZIndex,
      minWidth: `${colSize}`,
      minHeight: `${rowSize}`,
      borderBottom: "1px solid grey",
      borderRight: "1px solid grey",
    };

    return(

      <div className="table-grid-div"
        style={gridStyle}>
        <div className="zero-zero"
          style={zeroStyle}>
          <p>SevenRooms</p>
          <p>Coding Challenge</p>
        </div>
        <ColHeader
          timeData = {compiledTimes}/>
        <RowHeader
          tableNames={tableArray}/>
        <ul className="table-grid-cols"
          style={colsStyle}>
          {timeRanges.map( (time, idx) => {
            return <li className="table-col"
              key={time}
              style={{
                gridRow: `1 / ${rows + 1}`,
                gridColumn: `${1 + idx}`,
                backgroundColor: "lightgrey",
              }}>
              <TableCol
              timeData = {compiledTimes[time]}
              idx = {idx}/>
            </li>;
          })}
        </ul>
        <ul className="table-grid-rows"
          style={rowsStyle}>
          {Object.keys(tables).map( (tableKey, idx) => {
            return <li className="table-row"
              key = {tableKey}
              style={{
                gridRow: `${1 + idx}`,
                gridColumn: `1 / ${cols + 1}`}}>
              <TableRow
              tableName = {tableKey}
              idx = {idx}
              reservations = {tables[tableKey]}
              timeRange = {this.state["timeData"]}/>
          </li>;
          })}
        </ul>
        {testReservations.map( (reservation) => {
          return <Reservation
            key={reservation.name + reservation.time}
            reservation={reservation}
            startTime={startTime}
            tables={Object.keys(tables)}
            />;
        })}
      </div>
    );
  }
}


export default TableGrid;
