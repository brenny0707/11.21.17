import React from 'react';
const RowHeader = (props) => {
  const { tableNames } = props;
  const style = {
    gridColumn: "1",
    gridRow: "2 / -1 ",
    display: "grid",
    gridTemplateRows: `repeat(${tableNames.length}, 40px)`,
    borderBottom: "1px solid grey",
    borderRight: "1px solid grey",
    backgroundColor: "white",
    position: "sticky",
    left: "0px",
    zIndex: "9",

  };
  return (
    <div className="row-header"
      style={style}>
      {tableNames.map( (table) => {
        return <div
          className="table-header"
          key={table}>
          <div>{table}</div>
        </div>;
      })}
    </div>
  );
};

export default RowHeader;
