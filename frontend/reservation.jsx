import React from 'react';
import {timeRange} from './util_functions';
class Reservation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      reservation: props.reservation,
      startTime: props.startTime,
      tables: props.tables,
      toggled: false,
    };
    this.updateToggle = this.updateToggle.bind(this);
  }

  updateToggle() {
    this.setState((prevState) => {
      return { toggled: !prevState.toggled};
    });
  }

  render() {
    const {reservation, startTime, tables} = this.state;
    const bordered = this.state["toggled"] ? "reservation toggled" : "reservation";
    return <div
      className={bordered}
      onClick={this.updateToggle}
      style={{
        gridRow: `${ tables.indexOf(reservation["table"]) + 2 } / span 1`,
        gridColumn: `${ timeRange(startTime, reservation["time"]) / 15 + 2 } / span ${reservation["duration"] / 15}`,
        backgroundColor: "lightblue" ,
        height: "80%",
        alignSelf: "center",
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        zIndex: "2",
        borderRadius: "10px"
      }}>
      <div>{reservation.numGuests}</div>
      <div>{reservation.name}</div></div>;
    }
}


export default Reservation;
