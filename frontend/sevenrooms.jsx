import React from 'react';
import ReactDOM from 'react-dom';
import TableGrid from './table_grid';
import { compileReservations, timeRange, latestTime, mapTimes, compileTimeInfo } from './util_functions';

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");

const sampleReservations = [
  {
    name: 'Hurcules McHurculean',
    time: '8:00 PM',
    numGuests: 4,
    duration: 120,
    table: 'A'
  },
  {
    name: 'Johnny Bravo',
    time: '5:30 PM',
    numGuests: 2,
    duration: 60,
    table: 'B'
  },
  {
    name: 'Jon Snow',
    time: '6:00 PM',
    numGuests: 8,
    duration: 240,
    table: 'C'
  },
  {
    name: 'Donatello McTurtle',
    time: '12:30 AM',
    numGuests: 4,
    duration: 150,
    table: 'A'
  },
  {
    name: 'Elon Musket',
    time: '9:00 PM',
    numGuests: 4,
    duration: 120,
    table: 'M',
  },

  //own additions

  {
    name: 'Brendan Ko',
    time: '7:15 PM',
    numGuests: 4,
    duration: 105,
    table: 'D',
  },

  {
    name: 'Cool Guy',
    time: '6:00 PM',
    numGuests: 12,
    duration: 90,
    table: 'F',
  },

  {
    name: 'Some Guy',
    time: '5:30 PM',
    numGuests: 3,
    duration: 135,
    table: 'O',
  },

  {
    name: 'Scooby Doo',
    time: '8:00 PM',
    numGuests: 2,
    duration: 120,
    table: 'G',
  },

  {
    name: 'Halo Top',
    time: '5:15 PM',
    numGuests: 6,
    duration: 90,
    table: 'E',
  },

  {
    name: 'Jon Doon',
    time: '7:00 PM',
    numGuests: 12,
    duration: 180,
    table: 'E',
  },

  {
    name: 'Mary Doe',
    time: '11:00 PM',
    numGuests: 4,
    duration: 90,
    table: 'J',
  },

  {
    name: 'Joe Shmoe',
    time: '10:00 PM',
    numGuests: 12,
    duration: 105,
    table: 'H',
  },

];

const sampleTables = compileReservations(alphabet, sampleReservations);

const startTime = "5:00 PM";
const givenEndTime = "1:00 AM";
const endTime = latestTime(sampleReservations, startTime, givenEndTime);

const timeDuration = timeRange(startTime, endTime);

const timeRanges = mapTimes(startTime, timeDuration);
const compiledTimes = compileTimeInfo(timeRanges, sampleReservations);

const times = {
  startTime: startTime,
  endTime: endTime,
  timeDuration: timeDuration,
  timeRanges: timeRanges,
  compiledTimes: compiledTimes,
};

class Root extends React.Component {
  render() {
    return (
      <div className="sevenrooms-div">
        <TableGrid
          testReservations={sampleReservations}
          tables={sampleTables}
          timeData={times}/>
      </div>
    );
  }
}

document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(<Root/>, document.getElementById('main'));
});
