import React from 'react';
class TableCol extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      time: props.timeData["time"],
      reservationCount: props.timeData["reservationCount"],
      guestCount: props.timeData["guestCount"],
      idx: props.idx,
    };
  }

  render() {

    const {idx, time, reservationCount, guestCount} = this.state;
    const colStyle = {
      gridColumn: `${idx} / span 1`,
    };
    return (
      <div className="table-col-div"
        style={colStyle}>
      </div>
    );
  }
}

export default TableCol;
