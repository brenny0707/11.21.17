import React from 'react';
class TableRow extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: props.tableName,
      idx: props.idx,
      reservations: props.reservations,
      timeRange: props.timeRange,
    };
  }

  render() {

    const {name, reservations, timeRange, idx} = this.state;
    const rowStyle = {
      gridRow: `${idx}/ span 1`,
      display: `grid`,
    };
    return (
      <div className="table-row-div"
        style={rowStyle}>
      </div>
    );
  }
}
export default TableRow;
