

export const compileReservations = (keys, reservations) => {
  let compiled = {};

  keys.forEach((ch) => {
    compiled[ch] = {};
    });

  reservations.forEach( (reservation) => {
    let ch = reservation["table"];
    compiled[ch][reservation["time"]] = reservation;
  });
  return compiled;
};

export function timeConversion(givenTime) {
  const timeSplit = givenTime.split(" ");
  const ampm = timeSplit[1];
  const splitted = timeSplit[0].split(":");
  let hour = parseInt(splitted[0]);
  let minute = parseInt(splitted[1]);
  if (hour === 12) {
    hour = 0;
  }
  hour = ampm === "PM" ? (hour + 12) * 60 : hour * 60;
  return hour + minute;
}

function minuteConversion(minuteDuration) {
  let minutes = minuteDuration % 1440;
  let ampm = "AM";
  let hour = 0;
  while ( minutes >= 60) {
    hour += 1;
    minutes -= 60;
  }
  if ( hour >= 12 ) {
    ampm = "PM";
    hour -= 12;
  }
  hour = hour === 0 ? 12 : hour;
  minutes = minutes < 10 ? `0${minutes}` : `${minutes}`;
  return `${hour}:${minutes} ${ampm}`;
}

export const timeRange = (start, end) => {
  let startConverted = timeConversion(start);
  let endConverted = timeConversion(end);
  return startConverted > endConverted ? (1440 - startConverted) + endConverted : endConverted - startConverted;
};

export const latestTime = (reservations, start, end) => {
  let endTime = end;
  let latest = timeRange(start, endTime);
  reservations.forEach( (reservation) => {
    let reservationEnd = timeRange(start, reservation["time"]);
    if ( (reservationEnd + reservation["duration"] > latest) ) {
      endTime = minuteConversion(timeConversion(reservation["time"]) + reservation["duration"]);
      latest = timeRange(start, endTime);
    }
  });
  return endTime;
};

export const leftPosition = (start, reservationTime) => {
  let reservationMinutes = timeConversion(reservationTime);
  let startingMinutes = timeConversion(start);
  return reservationMinutes < startingMinutes ? ((1440 - startingMinutes) + reservationMinutes) * 4 : (reservationMinutes - startingMinutes) * 4;
};

export const mapTimes = (start, duration) => {
  let timePoints = [start];
  let currentTime = timeConversion(start);
  while (duration > 0) {
    currentTime += 15;
    duration -= 15;
    timePoints.push(minuteConversion(currentTime));
  }
  return timePoints;
};

export const compileTimeInfo = (times, reservations) => {
  let compiled = {};
  times.forEach( (time) => {
    compiled[time] = {
      time: time,
      reservationCount: 0,
      guestCount: 0,
    };
  });

  reservations.forEach( (reservation) => {
    compiled[reservation["time"]]["reservationCount"] += 1;
    compiled[reservation["time"]]["guestCount"] += reservation["numGuests"];
  });
  return compiled;
};
