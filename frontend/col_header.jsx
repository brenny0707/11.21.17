import React from 'react';
const ColHeader = (props) => {
  const { timeData } = props;
  const times = Object.keys(props.timeData);
  const style = {
    gridColumn: "2 / -1",
    gridRow: "1",
    display: "grid",
    gridTemplateColumns: `repeat(${times.length}, 90px)`,
    borderBottom: "1px solid grey",
    borderRight: "1px solid grey",
    backgroundColor: "white",
    position: "sticky",
    top: "0px",
    zIndex: "9",

  };
  return (
    <div className="col-header"
      style={style}>
      {times.map( (time) => {
        const timeInfo = timeData[time];
        return <div
          className="time-header"
          key={time}>
          <p>{timeInfo.time}</p>
          <p>Reservations: {timeInfo.reservationCount}</p>
          <p>Guests: {timeInfo.guestCount}</p>
        </div>;
      })}
    </div>
  );
};

export default ColHeader;
